## How to run the application?
1. git clone https://gitlab.com/nikhilsachdeva57/sleektask
2. cd sleektask
3. npm install
4. npm start

## Description
This is a web-app that receives data from https://www.hatchways.io/api/assessment/students and shows details of students. The user can search for students based on first and last name and the list will be filtered accordingly. The expanded list will show test mark details of each student

## Technical Details
Framework used : React.js
Libraries used : Axios (for GET query), Material UI(for views)