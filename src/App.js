import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import TextField from '@material-ui/core/TextField';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import './App.css';

export default class App extends React.Component {
  state = {
    input: '',
    persons: [],
    view: []
  }


  componentDidMount() {
    axios.get("https://www.hatchways.io/api/assessment/students")
      .then(res => {
        const persons = res.data.students;
        this.setState({ persons });
        console.log(this.state.persons)

        });

   
 
  }





   
   onChangeHandler = (e)=>{
    this.setState({
      input: e.target.value,
    })
    console.log(this.state.input)
  };



  render(){


    const list = this.state.persons
    .filter(d => this.state.input === '' || d.firstName.toLowerCase().includes(this.state.input.toLowerCase()) || d.lastName.toLowerCase().includes(this.state.input.toLowerCase()))
    .map((d, index) => {  
      let average = 0;
        for(var i=0; i<d.grades.length; i++){
            average += parseInt(d.grades[i])
        }
        average/=d.grades.length
        return(
    <div style={{"width" : "100%", "marginTop" : "20px", "alignItems" : "center"}}>
     
     <ExpansionPanel style={{"width" : "70%", "border" : '1px solid rgba(0, 0, 0, .125),', "alignContent" : "center"}}>
    
    <ExpansionPanelSummary
      expandIcon={<ExpandMoreIcon />}
      aria-controls="panel1a-content"
      id="panel1a-header"
    >
        <Avatar style={{"width": 80, "height" : 80, "border-width" : '2px solid black'}} variant="rounded" src={d.pic}/>
      <div style={{"flex":"column", "marginLeft" : 100}}>
      <Typography style={{"fontSize": 40, "text-align" : "left"}}>{d.firstName} {d.lastName} </Typography>
      <Typography style={{"fontSize": 20, "color" : "#232323",  "text-align" : "left"}}>Email: {d.email} </Typography>
      <Typography style={{"fontSize": 20, "text-align" : "left"}}>Company: {d.company} </Typography>
      <Typography style={{ "fontSize": 20,  "text-align" : "left"}}>Skill: {d.skill} </Typography>
      <Typography style={{ "fontSize": 20, "text-align" : "left"}}>Average: {average}% </Typography>
      </div>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Typography style={{width: "100%"}}>
        {d.grades.map((marks, index) =>{
            return(
            <Typography style={{ "fontSize": 20, "text-align" : "center"}}> Test {index + 1} {marks}%</Typography>
            
            )
        })}
      </Typography>
    </ExpansionPanelDetails>
  </ExpansionPanel>
    </div> )});
  return (
    <div className="App">

<TextField style={{"width" : "80%", "margin" : 60}} required id="standard-required" label="Search by name"  onChange={this.onChangeHandler}/>
         
        <ul>
        {list}
        </ul>
    
    </div>
  );
      }
}
